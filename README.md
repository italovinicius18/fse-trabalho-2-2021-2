# FSE - Trabalho 2 - 2021-2

## Descrição

Trabalho 2 da matérias de Fundamentos de sistemas embarcados.

## Uso

Para executar os programas basta clonar o repositório por meio do comando abaixo:

```sh
git clone https://gitlab.com/italovinicius18/fse-trabalho-2-2021-2.git
```

A partir do comando acima os dois sistemas serão clonados, tanto o sistema distribuído quando o central

### Executar sistema central

Basta acessar a pasta central por meio do comando:

```sh
cd central/
```

Compilar o programa por meio do comando abaixo:

```sh
make
```

Executar o sistema:

```sh
./bin/bin jsonfiles/configuracao_andar_terreo.json
```

O arquivo de entrada pode ser tanto do térreo quanto do primeiro andar, pois o ip do servidor central e a porta são os mesmos.

### Executar sistema distribuído

Basta acessar a pasta distribuído por meio do comando:

```sh
cd distribuido/
```

Compilar o programa por meio do comando abaixo:

```sh
make
```

Executar o sistema:

```sh
./bin/bin jsonfiles/configuracao_andar_terreo.json
```

A partir do comando acima o sistema apenas do térreo estará funcionando, porém podemos instanciar em outro terminal outro comando para iniciar o sistema do primeiro andar por meio do comando:

```sh
./bin/bin jsonfiles/configuracao_andar_1.json
```

## Authors and acknowledgment

Ítalo Vinícius.
