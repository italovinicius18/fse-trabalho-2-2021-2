#ifndef __DADOS_HPP__
#define __DADOS_HPP__

#include <string>
#include <vector>
using namespace std;

typedef struct Data {
    string type, tag;
    bool value;
} Data;

bool operator==(const Data& lhs, const Data& rhs);

typedef struct DistServidor {
    string ip, name;
    int port;
    double temperature, humidity;
    vector<Data> in_data, out_data;
} DistServidor;

typedef struct Event {
    Data data;
    DistServidor origin;
    int priority;
} Event;

#endif