#ifndef __FILA_HPP__
#define __FILA_HPP__

#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <mutex>
#include <deque>
using namespace std;

#include "Dados.hpp"

// this is a singleton class
class Fila {
public:
    static Fila* getInstance();
    static void destroyInstance();
    void push(Event event);
    Event pop();
    int size();
private:
    Fila() = default;
    ~Fila() = default;
    static Fila* instance;
    mutex queue_mtx;
    deque<Event> queue;
};


#endif // __FILA_HPP__