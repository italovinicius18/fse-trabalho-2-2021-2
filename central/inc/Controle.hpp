#ifndef __CONTROLE_HPP__
#define __CONTROLE_HPP__

#include "Fila.hpp"
#include "Dados.hpp"
#include "Servidor.hpp"
#include "Comunica.hpp"

#include <thread>
#include <iostream>
using namespace std;

extern int people_count;

class Controle {
private:
    static bool is_running;
    static bool is_alarm_on;
    void handle_event_smoke(Event event);
    void handle_event_presence(Event event);
    void handle_event_count(Event event);
    void handle_event_action(Event event);
public:
    Controle(){};
    ~Controle(){};
    void static stop_all();
    void init_controle();
    void static update_alarm(bool new_alarm_value);
    bool static get_alarm_status();
};

#endif // __CONTROLE_HPP__
