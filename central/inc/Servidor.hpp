#ifndef __SERVIDOR_HPP__
#define __SERVIDOR_HPP__

#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <mutex>
using namespace std;

#include "Dados.hpp"
#include "Fila.hpp"

// this is a singleton class
class Servidor {
public:
    static Servidor* getInstance();
    static void destroyInstance();
    void addServidor(const DistServidor & servidor);
    void removeServidor(const string & name);
    DistServidor getServidor(const string& name);
    vector<DistServidor> getServidor();
    bool hasServidor(const string& name);
    void updateTemperature(const string & name, 
    const double & temperature, const double & humidity);
    void updateData(const string& name, Data data);
private:
    Servidor() = default;
    ~Servidor();    
    static Servidor* instance;
    mutex servidor_mtx;
    unordered_map<string, DistServidor> servidor;
};


#endif