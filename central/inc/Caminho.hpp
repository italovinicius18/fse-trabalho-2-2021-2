#ifndef __CAMINHO_HPP__
#define __CAMINHO_HPP__

#include <string>
#include <unordered_set>
#include <iostream>
using namespace std;

#include "cJSON.h"
#include "Dados.hpp"
#include "Servidor.hpp"
#include "CaminhoSem.hpp"

static unordered_set<string> VALID_DADOS = {"init", "leave", "data"};

class Caminho {
public:
    Caminho(cJSON * request);
    ~Caminho() = default;
    void filterRequest();
private:
    cJSON * request;
    void init();
    void leave();
    void data();
};

#endif // __CAMINHO_HPP__