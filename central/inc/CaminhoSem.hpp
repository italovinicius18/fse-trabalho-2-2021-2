#ifndef __CAMINHO_SEM_HPP__
#define __CAMINHO_SEM_HPP__

#include <semaphore.h>
#include <iostream>
using namespace std;

#define MAX_SEM_COUNT 5

class CaminhoSem {
private:
    static sem_t sem;
public:
    static void init();
    static void acquire();
    static void release();
};

#endif  