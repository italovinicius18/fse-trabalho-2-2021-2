#include "Interface.hpp"

void Interface::operator()(bool & is_running) {
    // init ncurses
    initscr();
    start_color();
    cbreak();
    nonl();               // Get return key
    keypad(stdscr, 1);    // Fix keypad
    noecho();             // No automatic printing
    curs_set(0);          // Hide real cursor
    timeout(200);
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_CYAN, COLOR_BLACK);

    int max_y, max_x;
    getmaxyx(stdscr, max_y, max_x);
    int win_l_y = max_y / 2 - 3;
    int win_l_x = max_x / 2 - 20;
    WINDOW *win_upper = newwin(win_l_y, max_x-2, 2, 1);
    WINDOW *win_down = newwin(win_l_y, max_x-2, max_y/2, 1);

    int current_servidor_index = 0;
    int servidor_num = 0;

    bool first_time_empty = false;

    while(true) {
        Servidor * s = Servidor::getInstance();
        vector<DistServidor> all_states = s->getServidor();
        servidor_num = all_states.size();
        

        wmove(stdscr, 0, 0);
        wclrtoeol(stdscr);
        wmove(stdscr, 1, 0);
        wclrtoeol(stdscr);

        if (servidor_num == 0) {
            if(!first_time_empty) {
                first_time_empty = true;
                clear();
            }
            mvprintw(0, max_x/2 - 24, "Não há nenhum servidor distribuído conectado");
            mvprintw(1, max_x/2 - 7, "Aguardando...");
            attron(COLOR_PAIR(3));
            mvprintw(max_y - 2, 3, "Use as setas (<- e ->) para alternar entre os servidores");
            mvprintw(max_y - 1, 3, "Use 'q' para sair");
            attroff(COLOR_PAIR(3));
            refresh();

            int ch = getch();
            if(ch == 'q'){
                break;
            }
        } else {
            first_time_empty = false;
            DistServidor current_servidor = all_states[current_servidor_index%servidor_num];
            // create menu
            stringstream servidor_header;
            servidor_header << current_servidor.name
            << " (" << current_servidor_index + 1 << "/" << all_states.size() << ")";

            stringstream info3;
            info3 << fixed;
            info3 << setprecision(1);
            info3 << "T: " << current_servidor.temperature << " ºC";
            info3 << " H: " << current_servidor.humidity << " %";

            string info2 = current_servidor.ip + ":" + to_string(current_servidor.port);

            stringstream alarm_info; 
            alarm_info << "Alarme: " << (Controle::get_alarm_status() ? "Ligado" : "Desligado");

            mvprintw(0, max_x - info3.str().length(), info3.str().c_str());
            mvprintw(1, max_x - alarm_info.str().length(), alarm_info.str().c_str());
            mvprintw(0, 1, info2.c_str());
            mvprintw(0, max_x/2 - servidor_header.str().length()/2, servidor_header.str().c_str());
            
            // use instructions
            attron(COLOR_PAIR(3));
            mvprintw(max_y - 3, 3, "Use os números de 1 a 9 para alterar manualmente as saídas do servidor atual...");
            mvprintw(max_y - 2, 3, "Use as setas (<- e ->) para alternar entre os servidores");
            mvprintw(max_y - 1, 3, "Use 'a' para alternar o alarme, 'q' para sair");
            attroff(COLOR_PAIR(3));
            refresh();

            // TODO position data on vertical center of window
            werase(win_upper);
            box(win_upper, 0, 0);
            for(int i = 0; i < current_servidor.in_data.size(); i++){
                
                mvwprintw(win_upper, i+2, 2, current_servidor.in_data[i].tag.c_str());
                if(current_servidor.in_data[i].value){
                    wattron(win_upper, COLOR_PAIR(1));
                    mvwprintw(win_upper, i+2, win_l_x - 4, "LIG");
                    wattroff(win_upper, COLOR_PAIR(1));
                } else {
                    wattron(win_upper, COLOR_PAIR(2));
                    mvwprintw(win_upper, i+2, win_l_x - 5, "DESL");
                    wattroff(win_upper, COLOR_PAIR(2));
                }
            }
            wrefresh(win_upper);

            werase(win_down);
            box(win_down, 0, 0);
            for(int i = 0; i < current_servidor.out_data.size(); i++){
                stringstream tag_prefix;
                tag_prefix << "(" << i + 1 << ") "
                << current_servidor.out_data[i].tag;
                mvwprintw(win_down, i+2, 2, tag_prefix.str().c_str());
                if(current_servidor.out_data[i].value){
                    wattron(win_down, COLOR_PAIR(1));
                    mvwprintw(win_down, i+2, win_l_x - 4, "LIG");
                    wattroff(win_down, COLOR_PAIR(1));
                } else {
                    wattron(win_down, COLOR_PAIR(2));
                    mvwprintw(win_down, i+2, win_l_x - 5, "DESL");
                    wattroff(win_down, COLOR_PAIR(2));
                }
            }
            wrefresh(win_down);

            int ch = getch();
            char c = ch;
            if(ch == KEY_LEFT){
                current_servidor_index--;
                if(current_servidor_index < 0)
                    current_servidor_index = servidor_num - 1;
                werase(win_upper);
            } else if(ch == KEY_RIGHT){
                current_servidor_index++;
                if(current_servidor_index >= servidor_num)
                    current_servidor_index = 0;
                werase(win_upper);
            } else if(ch == KEY_F(1)){
                break;
            } else if(ch == KEY_F(2)){
                Controle::update_alarm(!Controle::get_alarm_status());
                werase(win_down);
            } else if(isdigit(c) and (c - '0') < (current_servidor.out_data.size()+1)) {
                mvprintw(max_y - 4, 3, "Output (%c) alterado manualmente!", c);
                Data current_state = current_servidor.out_data[c - '1'];
                Data new_state = {"action", current_state.tag, !current_state.value};
                Event e = {new_state, current_servidor, 0};
                Fila * fila = Fila::getInstance();
                fila->push(e);
            }
            
        }

        this_thread::sleep_for(10ms);
    } 

    delwin(win_upper);
    endwin();
    is_running = false;
}