#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <streambuf>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <signal.h>
#include <thread>
using namespace std;

#include "cJSON.h"
#include "Dados.hpp"
#include "Caminho.hpp"
#include "CaminhoSem.hpp"
#include "Interface.hpp"
#include "Controle.hpp"
#include "Comunica.hpp"

int sockfd;

#define MAX_BUFFER_SIZE 2048

void set_server_addr(struct sockaddr_in * serv_addr, char * addr, char * port) {
  int errcode;
  struct addrinfo hints, *result;

  memset(&hints, 0, sizeof (hints));

  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  errcode = getaddrinfo (addr, NULL, &hints, &result);

  if (errcode != 0) {
    perror ("getaddrinfo");
    return;
  }

  serv_addr->sin_family = AF_INET;
  auto * temp_addr = (struct sockaddr_in *) result->ai_addr;
  serv_addr->sin_addr = temp_addr->sin_addr; 
  serv_addr->sin_port = htons(atoi(port));
}

int is_ready(int fd) {
  fd_set fdset;
  struct timeval timeout;
  FD_ZERO(&fdset);
  FD_SET(fd, &fdset);

  timeout.tv_sec = 0;
  timeout.tv_usec = 1;

  return select(fd+1, &fdset, NULL, NULL, &timeout) == 1;
}


int main(int argc, char *argv[]){
    if (argc < 2){
        printf("Aqui pode utilizar qualquer um dos arquivos json\n");
        printf("Exemplo: ./bin/bin jsonfiles/configuracao_andar_terreo.json\n");
        exit(0);
    }

    string config_file = argv[1];
    ifstream t(config_file);
    if (!t.good()) {
        printf("Falha ao acessar arquivo\n");
        exit(0);
    }
    string config_str((istreambuf_iterator<char>(t)),
                istreambuf_iterator<char>());
    t.close();
    

    cJSON *config = cJSON_Parse(config_str.c_str());
    char *ip;
    int server_port;

    ip = cJSON_GetObjectItem(config, "ip_servidor_central")->valuestring;
    server_port = cJSON_GetObjectItem(config, "porta_servidor_central")->valueint;
    
    string server_port_string = to_string(server_port);
    char* server_port_char = new char[server_port_string.length() + 1];
    strcpy(server_port_char, server_port_string.c_str());
    signal(SIGINT, [](int sig) {
        close(sockfd);
        exit(0);
    });
    struct sockaddr_in serv_addr;
    memset((char *) &serv_addr, 0, sizeof(serv_addr));
    set_server_addr(&serv_addr, ip, server_port_char);
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        cout << "Falha ao abrir socket" << endl;
        return 1;
    }
    cout << "Servidor central iniciado!" << endl;
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        cout << "Falha no bind" << endl;
        return 1;
    }
    
    listen(sockfd, 5);

    bool is_running = true;

    CaminhoSem::init();
    comunica_sem_init();

    Interface interface;

    thread interface_thread(interface, ref(is_running));

    interface_thread.detach();

    Controle * controle = new Controle();

    thread controle_thread(&Controle::init_controle, controle);

    controle_thread.detach();

    while(is_running){
        // accept connections and print to stdout
        struct sockaddr_in cli_addr;
        socklen_t clilen = sizeof(cli_addr);
        if(!is_ready(sockfd)) continue; 
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) {
            cout << "Falha no accept" << endl;
            return 1;
        }

        char* buffer = new char[MAX_BUFFER_SIZE];
        bzero(buffer, MAX_BUFFER_SIZE);
        // TODO - read all data from socket, allocating memory if necessary
        int n = read(newsockfd, buffer, MAX_BUFFER_SIZE);
        if (n < 0) {
            cout << "Falha na leitura do socket" << endl;
            return 1;
        }
        close(newsockfd);
        cJSON * root = cJSON_Parse(buffer);
        if (root == NULL) {
            cout << "Falha ao fazer o parse do JSON" << endl;
            return 1;
        }
        string ip = inet_ntoa(cli_addr.sin_addr);
        cJSON_AddStringToObject(root, "ip", ip.c_str());
        CaminhoSem::acquire();
        Caminho * caminho = new Caminho(root);
        thread * caminho_thread = new thread(&Caminho::filterRequest, caminho);
        caminho_thread->detach();
    }

    Controle::stop_all();

    close(sockfd);

    return 0;
}