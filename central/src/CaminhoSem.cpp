#include "CaminhoSem.hpp"

sem_t CaminhoSem::sem;

void CaminhoSem::init() {
    sem_init(&sem, 0, MAX_SEM_COUNT);
}

void CaminhoSem::acquire() {
    sem_wait(&sem);
}

void CaminhoSem::release() {
    sem_post(&sem);
}