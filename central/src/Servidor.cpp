#include "Servidor.hpp"

Servidor * Servidor::instance; 

Servidor::~Servidor(){
    this->servidor_mtx.~mutex();
}

Servidor* Servidor::getInstance(){
    if(Servidor::instance == nullptr){
        Servidor::instance = new Servidor();
    }
    return Servidor::instance;
}


void Servidor::destroyInstance(){
    if(Servidor::instance != nullptr){
        delete Servidor::instance;
        Servidor::instance = nullptr;
    }
}

void Servidor::addServidor(const DistServidor & servidor){
    lock_guard<mutex> lock(this->servidor_mtx);
    this->servidor[servidor.name] = servidor;
}

void Servidor::removeServidor(const string & name){
    lock_guard<mutex> lock(this->servidor_mtx);
    this->servidor.erase(name);
}

DistServidor Servidor::getServidor(const std::string & name){
    return this->servidor[name];
}

vector<DistServidor> Servidor::getServidor() {
    vector<DistServidor> res;
    if(!this->servidor.empty()) {
        lock_guard<mutex> lock(this->servidor_mtx);

        for(auto & servidor : this->servidor){
            res.push_back(servidor.second);
        }
    }
    return res;
}

void Servidor::updateData(const string & name, Data data){
    lock_guard<mutex> lock(this->servidor_mtx);
    auto it_in = find_if(this->servidor[name].in_data.begin(), 
    this->servidor[name].in_data.end(),
    [&](const Data &d) { return d.tag == data.tag and d.type == data.type; });
    auto it_out = find_if(this->servidor[name].out_data.begin(), 
    this->servidor[name].out_data.end(),
    [&](const Data &d) { return d.tag == data.tag and d.type == data.type; });

    if(it_in != this->servidor[name].in_data.end()){
        it_in->value = data.value;
    }
    if(it_out != this->servidor[name].out_data.end()){
        it_out->value = data.value;
    }

    // TODO Handle priority
    Event e = {data, this->servidor[name], 0};
    Fila * fila = Fila::getInstance();
    fila->push(e);
}

void Servidor::updateTemperature(const string & name, 
const double & temperature, const double & humidity) {
    lock_guard<mutex> lock(this->servidor_mtx);
    this->servidor[name].temperature = temperature;
    this->servidor[name].humidity = humidity;
}

bool Servidor::hasServidor(const string& name) {
    lock_guard<mutex> lock(this->servidor_mtx);
    return this->servidor.find(name) != this->servidor.end();
}