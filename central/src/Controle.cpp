#include "Controle.hpp"

bool Controle::is_running = true;
bool Controle::is_alarm_on = false;

int people_count = 0;

void Controle::stop_all() {
    is_running = false;
}

void Controle::update_alarm(bool new_alarm_value) {
    Controle::is_alarm_on = new_alarm_value;
}

bool Controle::get_alarm_status() {
    return is_alarm_on;
}

cJSON * mount_action (string tag, int value) {
    cJSON * request = cJSON_CreateObject();
    cJSON_AddStringToObject(request, "type", "action");
    cJSON * action = cJSON_CreateObject();
    cJSON_AddStringToObject(action, "tag", tag.c_str());
    cJSON_AddNumberToObject(action, "value", value);
    cJSON_AddItemToObject(request, "action", action);
    return request;
}

void Controle::handle_event_smoke(Event event){
    // TODO: Handle event
    // Turn on fire sprinkler
    if(event.data.value) {
        Servidor * servidor = Servidor::getInstance();
        vector<DistServidor> dist_servidor = servidor->getServidor();
        for(auto & dist_servidor : dist_servidor) {
            for(auto & out_data : dist_servidor.out_data) {
                if(out_data.type == "aspersor") {
                    cJSON * request = mount_action(out_data.tag, 1);
                    servidor->updateData(dist_servidor.name, {out_data.type, out_data.tag, true});
                    send_message_async(dist_servidor.ip, dist_servidor.port, request);
                }
            }

        }
    }
}

// receives type as "action" 
void Controle::handle_event_action(Event event) {
    cJSON * request = mount_action(event.data.tag, event.data.value);
    Servidor * servidor = Servidor::getInstance();
    for(auto & data : event.origin.out_data) {
        if(data.tag == event.data.tag) {
            servidor->updateData(event.origin.name, {data.type, data.tag, event.data.value});
        }
    }
    send_message_async(event.origin.ip, event.origin.port, request);
}

void Controle::handle_event_presence(Event event){
    if(Controle::is_alarm_on) {
        // TODO: Run Alarm sound
    } else if(event.data.type == "presenca") {
        // TODO : Check value and schedule to turn off after 10s 
        Data data;
        for(auto d : event.origin.out_data) {
            if(d.type == "lampada" and d.tag.find("Corredor") != string::npos)
                data = d;
        }

        cJSON * request = mount_action(data.tag, event.data.value);
        Servidor * servidor = Servidor::getInstance();
        servidor->updateData(event.origin.name, {data.type, data.tag, event.data.value});
        send_message_async(event.origin.ip, event.origin.port, request);
    }
}

void Controle::handle_event_count(Event event){
    if(event.data.tag.find("Entrando") != std::string::npos) {
        people_count++;
    } else if(event.data.tag.find("Saindo") != std::string::npos) {
        people_count > 0 and people_count--;
    }
}


void Controle::init_controle() {
    while(is_running) {
        Fila * fila = Fila::getInstance();
        while(fila->size() > 0) {
            Event event = fila->pop();
            if(event.data.type == "fumaca"){
                this->handle_event_smoke(event);
            }else if(event.data.type == "porta" or
            event.data.type == "janela" or
            event.data.type == "presenca"){
                this->handle_event_presence(event);
            }else if(event.data.type == "contagem") {
                this->handle_event_count(event);
            }else if(event.data.type == "action") {
                this->handle_event_action(event);
            }
        }

        this_thread::sleep_for(10ms);
    }
}