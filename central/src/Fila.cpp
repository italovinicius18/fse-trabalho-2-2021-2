#include "Fila.hpp"

// this is a singleton class

Fila * Fila::instance; 


Fila* Fila::getInstance(){
    if(Fila::instance == nullptr){
        Fila::instance = new Fila();
    }
    return Fila::instance;
}


void Fila::destroyInstance(){
    if(Fila::instance != nullptr){
        delete Fila::instance;
        Fila::instance = nullptr;
    }
}

void Fila::push(Event event){
    lock_guard<mutex> lock(this->queue_mtx);
    // TODO handle priority
    this->queue.push_back(event);
}

Event Fila::pop(){
    Event event;
    lock_guard<mutex> lock(this->queue_mtx);
    event = this->queue[0];
    this->queue.pop_front();
    return event;
}

int Fila::size(){
    lock_guard<mutex> lock(this->queue_mtx);
    return this->queue.size();
}
