#include "Caminho.hpp"

Caminho::Caminho(cJSON * request) {
    this->request = request;
}

void Caminho::init() {
    DistServidor dist_servidor;
    dist_servidor.ip = cJSON_GetObjectItem(request, "ip")->valuestring;
    dist_servidor.name = cJSON_GetObjectItem(request, "name")->valuestring;
    dist_servidor.port = cJSON_GetObjectItem(request, "port")->valueint;
    cJSON * in_data = cJSON_GetObjectItem(request, "inputs");
    cJSON * out_data = cJSON_GetObjectItem(request, "outputs");
    for(int i = 0; i < cJSON_GetArraySize(in_data); i++){
        cJSON * data = cJSON_GetArrayItem(in_data, i);
        Data d;
        d.type = cJSON_GetObjectItem(data, "type")->valuestring;
        d.tag = cJSON_GetObjectItem(data, "tag")->valuestring;
        d.value = false;
        dist_servidor.in_data.push_back(d);
    }
    for(int i = 0; i < cJSON_GetArraySize(out_data); i++){
        cJSON * data = cJSON_GetArrayItem(out_data, i);
        Data d;
        d.type = cJSON_GetObjectItem(data, "type")->valuestring;
        d.tag = cJSON_GetObjectItem(data, "tag")->valuestring;
        d.value = false;
        dist_servidor.out_data.push_back(d);
    }
    Servidor * servidor = Servidor::getInstance();
    if(servidor->hasServidor(dist_servidor.name)){
        cout << "O servidor distribuído com esse nome " << dist_servidor.name 
        << " já existe." << endl;
    } else {
        servidor->addServidor(dist_servidor);
    }
    cout << "O servidor distribuído " << dist_servidor.name 
    << " foi inserido." << endl;
}

void Caminho::data() {
    string servidor_name = cJSON_GetObjectItem(request, "servidor_name")->valuestring;
    cJSON * data = cJSON_GetObjectItem(request, "data");
    Servidor * servidor = Servidor::getInstance();
    Data req_data;
    req_data.type = cJSON_GetObjectItem(data, "type")->valuestring;
    req_data.tag = cJSON_GetObjectItem(data, "tag")->valuestring;
    
    if (req_data.type == "contagem") {
        req_data.value = cJSON_GetObjectItem(data, "value")->valueint;
    } else if (req_data.type == "temperature") {
        double temperature = cJSON_GetObjectItem(data, "temperature")->valuedouble;
        double humidity = cJSON_GetObjectItem(data, "humidity")->valuedouble;
        servidor->updateTemperature(servidor_name, temperature, humidity);
        return;
    } else {
        cJSON * state = cJSON_GetObjectItem(data, "state");
        req_data.value = cJSON_IsTrue(state) != 0;
    }
    servidor->updateData(servidor_name, req_data);
}

void Caminho::leave() {
    string name = cJSON_GetObjectItem(request, "name")->valuestring;
    Servidor * servidor = Servidor::getInstance();
    servidor->removeServidor(name);
    cout << "O servidor distribuído " <<  name << " foi removido." << endl;
}

void Caminho::filterRequest() {
    string type = cJSON_GetObjectItem(request, "type")->valuestring;
    if (VALID_DADOS.find(type) == VALID_DADOS.end()) {
        cerr << "Invalid request type: " << type << endl;
    }
    cout << "Request type: " << type << endl;
    if (type == "init") {
        init();
    } else if (type == "data") {
        data();
    } else if (type == "leave") {
        leave();
    }
    cout << "Request processed" << endl;
    CaminhoSem::release();
}
