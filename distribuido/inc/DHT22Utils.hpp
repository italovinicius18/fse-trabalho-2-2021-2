#ifndef __DHT22UTILS_HPP__
#define __DHT22UTILS_HPP__

#include <iostream>
#include <thread>
using namespace std;

extern "C" {
    #include "dht22.h"
}

#include "Comunica.hpp"
#include "cJSON.h"

class DHT22UTILS {
public:
    void operator()(bool & is_end); 
};


#endif // __DHT22UTILS_HPP__

